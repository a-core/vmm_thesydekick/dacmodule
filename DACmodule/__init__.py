"""
=========
DACmodule
=========

DACmodule model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, youu may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *

import numpy as np

class DAC(thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        sys.stdout.flush()
        self.proplist = [ 'Rs' ];    # Properties that can be propagated from parent
        self.Rs =  100e6;            # Sampling frequency
        self.IOS=Bundle()            # Pointer for input data
        self.IOS.Members['d_in']=IO()   # Pointer for input data
        self.IOS.Members['d_type'] = IO() # data type: 1->weights, 0->image
        self.IOS.Members['d_out'] = IO()  # Pointer for ouput data

        self.model='py';             # Can be set externally, but is not propagated
        self.par= False              # By default, no parallel processing
        self.queue= []               # By default, no parallel processing
        self.sign_flag = 1
        self.test_flagw = False
        self.test_flagi = False

        if len(arg)>=1:
            parent=arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent =parent

        self.init()

    def init(self):
        pass #Currently nohing to add

    def main(self):
        '''Guideline. Isolate python processing to main method.
        
        To isolate the interna processing from IO connection assigments, 
        The procedure to follow is
        1) Assign input data from input to local variable
        2) Do the processing
        3) Assign local variable to output

        '''
        if self.IOS.Members['d_type'].Data == 1:
            data_ = self.int8_float(isWeights=True)
        else:
            data_ = self.int8_float(isWeights=False)
        
        self.IOS.Members['d_out'].Data = data_

    def run(self,*arg):
        '''Guideline: Define model depencies of executions in `run` method.
        '''
        if len(arg)>0:
            self.par=True      #flag for parallel processing
            self.queue=arg[0]  #multiprocessing.queue as the first argument
        if self.model=='py':
            self.main()

    def __str__(self) -> str:
        return "this is DAC module"

    def int8_float(self, isWeights=False):
        """
        main process to decode message as numeric number
        e.g. int 32->0.5 in image data
        param isWeights: True: weights, False: image data
        return float number result
        """
        #print(self.IOS.Members['d_in'].Data) ## debug 
        
        
        
        
        try:
            message = int(str(self.IOS.Members['d_in'].Data),2)
        except ValueError:
            message = 0
        if isWeights:
            if (message >= 128):
                sign_flag = -1
                message -= 128 # Remove sign
            elif (message >= 64):
                sign_flag = 1
                message -= 64 # Remove sign
            else:
                sign_flag = 0
        else:
            if (message >= 128):
                message -= 128 # Remove the bit since it's not used
            if (message >= 64):
                message -= 64 # Remove sign
                sign_flag = -1
            else:
                sign_flag = 1

        message = message*(1/63)*sign_flag
        
        #print(" TESTING !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        #print("Processed DAC Output (Analog Value):", message)
        
        return message
        


if __name__=="__main__":
    import matplotlib.pyplot as plt
    from  DACmodule import *
    from  DACmodule.controller import controller as DACmodule_controller
    import pdb
    import math
    length=1024
    rs=100e6
    indata=np.cos(2*math.pi/length*np.arange(length)).reshape(-1,1)

    models=[ 'py']
    duts=[]
    for model in models:
        d=DAC()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.init()

        d.IOS.Members['d_in'].Data = '1000110'
        d.IOS.Members['d_type'].Data = 0
        d.run()
        print("anwser: ", d.IOS.Members['d_out'].Data)

    input()


